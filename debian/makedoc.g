# debian/makedoc.g -- GAP script
SetPackagePath("factint",".");
LoadPackage("autodoc");;
LoadPackage("factint");;
AutoDoc(rec(
	scaffold := rec(
		includes := [ "preface.xml", "general.xml", "methods.xml", "timings.xml" ],
		bib := "factintbib.xml",
		gapdoc_latex_options := rec( EarlyExtraPreamble := """
			\usepackage{amsfonts}
			\usepackage{amsxtra}
			""" ),
		)
	));
