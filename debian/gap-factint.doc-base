Document: gap-factint
Title: FactInt: Advanced Methods for Factoring Integers
Author: Stefan Kohl <stefan@mcs.st-and.ac.uk>
Abstract:
 This package for GAP provides a general-purpose integer factorization
 routine, which makes use of a combination of factoring methods.
 In particular it contains implementations of the following algorithms:
  - Pollard's p-1
  - Williams' p+1
  - Elliptic Curves Method (ECM)
  - Continued Fraction Algorithm (CFRAC)
  - Multiple Polynomial Quadratic Sieve (MPQS)
 .
 It also contains code by Frank Lübeck for making use of Richard P. Brent's
 tables of factors of integers of the form b^k ± 1. FactInt is completely
 written in the GAP language and contains / requires no external binaries.
Section: Science/Mathematics

Format: pdf
Files: /usr/share/doc/gap-factint/doc/manual.pdf

Format: HTML
Index: /usr/share/doc/gap-factint/doc/chap0.html
Files: /usr/share/doc/gap-factint/doc/*.html

Format: text
Index: /usr/share/doc/gap-factint/doc/chap0.txt
Files: /usr/share/doc/gap-factint/doc/*.txt
